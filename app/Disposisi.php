<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Disposisi extends Model
{
    protected $table = "disposisi";
    protected $fillable = [
        "id","surat_masuk_id","teruskan","catatan","bagikan","created_at","update_at"
    ];
    
     public function surat_masuk(){
         return $this->belongsTo(Surat_masuk::class);

     }


}
