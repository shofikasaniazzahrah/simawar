<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Disposisi;
use App\Surat_masuk;
use Illuminate\Support\Facades\Auth;

class DisposisiController extends Controller
{
    public function index()
    {

        $getdisposisi = Disposisi::all();
        $data_user = Auth::user()->id;
        return view('pages.disposisi', compact('data_user', 'getdisposisi'));
    }
    public function create()
    {
        $data_user = Auth::user()->id;
        return view('pages.form-disposisi', compact('data_user'));
    }

    public function destroy($id)
    {
        $query = Disposisi::find($id);
        $query->delete();
        return redirect('/view/surat/' . $query->surat_masuk_id);
    }

    public function store(Request $request)
    {
        $request->validate([
            'catatan'=>'required',
            'teruskan'=>'required',
            
        ]);

        $teruskan = $request->input('teruskan');
        // dump(implode(", ",$teruskan));
        Disposisi::create([

            'surat_masuk_id' => $request->id_surat,
            'teruskan' => implode(", ", $teruskan),
            'catatan' => $request->catatan,
        ]);

        return redirect('/view/surat/' . $request->id_surat);
    }

    public function update($id, Request $request)
    {
        //  dump($request);
        $teruskan = $request->input('teruskan');

        $change = Disposisi::find($id);
        $change->teruskan = implode(", ", $teruskan);
        $change->catatan = $request["catatan"];


        $change->update();
        return redirect('/view/surat/' . $change->surat_masuk_id);
    }

    public function admin()
    {
        $get_admin = Disposisi::all();
        $data_user = Auth::user()->id;
        // dump($getdisposisi);
        return view('pages.disposisi-admin', compact('data_user', 'get_admin'));
    }

    public function edit($id)
    {
        $data_id =  Disposisi::find($id);
        // dump(explode(", ",$data_id->teruskan));     
        $array = [
            "Sekretaris",
            "Kabid Pelayanan Terpadu Satu Pintu",
            "Kabid Sistem Informasi Evaluasi dan Pengaduan Layanan",
            "Kabid Pengendalian layanan informasi",
            "Kabid Deregulasi Pengembangan Iklim dan Promosi Penanaman Modal",
            "Kasubag",
            "Kasi-kasi",
            "Staff"
        ];
        $data_teruskan = explode(", ", $data_id->teruskan);
        $data_tidakteruskan = array_diff($array, $data_teruskan);
        // dump($data_tidakteruskan);
        // dump($data_teruskan);
        // dump($array);
        $data_user = Auth::user()->id;
        $data_jabatan = '';
        return view('pages.edit-disposisi', compact('data_user','data_id', 'data_teruskan', 'data_tidakteruskan'));
    }
}
