<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $jmlh_surat_masuk = DB::table('surat_masuk')->count();
        $jmlh_disposisi = DB::table('disposisi')->count();
        $jmlh_users = DB::table('users')->count();
        $data_user = Auth::user()->id;
        // dump($sum_surat_masuk);
        $jmlh_surat_keluar = DB::table('surat_keluar')->count();
        
        return view('pages.home', compact('data_user','jmlh_disposisi','jmlh_surat_masuk', 'jmlh_surat_keluar','jmlh_users'));

    }
}
