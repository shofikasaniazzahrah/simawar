<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Surat_keluar;
use Illuminate\Support\Facades\Auth;
use Storage;


class SuratKeluarController extends Controller
{
    public function index(){
        $suratkeluar = DB::table('surat_keluar')->get();
        $suratkeluar_2 = DB::table('surat_keluar')->first();
        $data_user = Auth::user()->id;
        $extension = '';
        return view ('pages.surat-keluar', compact('data_user','suratkeluar','extension') );
     
    }
    public function create(){
        $data_user = Auth::user()->id;
        return view ('pages.form-surat-keluar', compact('data_user'));
    }

    public function destroy($id){
        $query = DB::table('surat_keluar')->where('id',$id)->delete();
        return redirect('surat-keluar');
    }
    
    public function store(Request  $request){

        $request->validate([
            'nomor_surat'=>'required',
            'tujuan_surat'=>'required',
            'perihal'=>'required',
            'keterangan'=>'required',
            'file'=>'required',
        ]);
        // dump($request);
        $file='statis'; 
        $file=time().'_'.$request["file"]->getClientOriginalName();
       
        $destination = $request["file"]->storeAS('upload', $file, 'public');
        

        Surat_keluar::create([
            "nomor_surat" =>  $request["nomor_surat"],
            "tujuan_surat" =>  $request["tujuan_surat"],
            "perihal" =>  $request["perihal"],
            "keterangan" =>  $request["keterangan"],
            "file" =>  $destination,

        ]);
        return redirect('/surat-keluar')->with('berhasil',' Data Baru Berhasil Disimpan!');
    }

    public function download(Request $request){
        $file = public_path('/storage/'.$request["file"]);
        return response()->download($file);
    }
    
    public function edit($id){
        $data_user = Auth::user()->id;
        $data_id = DB::table('surat_keluar')->where('id', $id)->first();
        
        return view('pages.edit-surat-keluar', compact('data_user','data_id'));
    }
 

    public function update($id, Request $request){
        $change = Surat_keluar::find($id);
        $change->nomor_surat = $request["nomor_surat"];
        $change->tujuan_surat = $request["tujuan_surat"];
        $change->perihal = $request["perihal"];
        $change->keterangan = $request["keterangan"];
        // $change->file = $destination;
        $change->update();
        return redirect('/surat-keluar');

    }

        public function updatefile($id, Request $request){
            $new_file= time().'_'.$request["file"]->getClientOriginalName();
            $destination = $request["file"]->storeAS('upload', $new_file, 'public');  
    
            $change = Surat_keluar::find($id);
            $change->file = $destination;
            $change->update();
            return redirect('/surat-keluar');
    
        }
       
    
}
