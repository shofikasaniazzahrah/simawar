<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Surat_masuk;
use Storage;

use App\Disposisi;
use Illuminate\Support\Facades\Auth;

class SuratMasukController extends Controller
{
    public function index()
    {
        $suratmasuk = DB::table('surat_masuk')->get();
        $suratmasuk_2 = DB::table('surat_masuk')->first();
        $extension = '';
        // dd($extension);

        $data_user = Auth::user()->id;
        return view ('pages.surat-masuk', compact('data_user','suratmasuk','extension') );
    }

    public function create()
    {
        $data_user = Auth::user()->id;
        return view('pages.form-surat-masuk', compact('data_user'));
    }

    public function destroy($id)
    {
        $query = DB::table('surat_masuk')->where('id', $id)->delete();
        return redirect('surat-masuk');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nomor_surat'=>'required',
            'asal_surat'=>'required',
            'tanggal_surat'=>'required',
            'perihal'=>'required',
            'keterangan'=>'required',
            'file'=>'required',
        ]);

        // dump($request);
        $disetujui = 'false';
        $file = time() . '_' . $request["file"]->getClientOriginalName();
        $destination = $request["file"]->storeAS('upload', $file, 'public');



        Surat_masuk::create([
            "nomor_surat" =>  $request["nomor_surat"],
            "asal_surat" =>  $request["asal_surat"],
            "tanggal_surat" =>  $request["tanggal_surat"],
            "perihal" =>  $request["perihal"],
            "keterangan" =>  $request["keterangan"],
            "disetujui" =>  $disetujui,
            "file" =>  $destination,

        ]);



        return redirect('/surat-masuk')->with('berhasil', ' Data BaruBerhasil Disimpan!');
    }

    public function download(Request $request)
    {
        $file = public_path('/storage/' . $request["file"]);
        return response()->download($file);
    }

    public function edit($id)
    {
        $data_id = DB::table('surat_masuk')->where('id', $id)->first();
        $data_user = Auth::user()->id;
        return view('pages.edit-surat-masuk', compact('data_user', 'data_id'));
    }

    public function update($id, Request $request)
    {
        //  dump($request);
        // $new_file= time().'_'.$request["file"]->getClientOriginalName();
        // $destination = $request["file"]->storeAS('upload', $new_file, 'public');  

        $change = Surat_masuk::find($id);
        $change->nomor_surat = $request["nomor_surat"];
        $change->asal_surat = $request["asal_surat"];
        $change->tanggal_surat = $request["tanggal_surat"];
        $change->perihal = $request["perihal"];
        $change->keterangan = $request["keterangan"];
        $change->disetujui = $request["disetujui"];
        // $change->file = $destination;
        $change->update();
        return redirect('/surat-masuk');
    }

    public function updatefile($id, Request $request)
    {
        $new_file = time() . '_' . $request["file"]->getClientOriginalName();
        $destination = $request["file"]->storeAS('upload', $new_file, 'public');

        $change = Surat_masuk::find($id);
        $change->file = $destination;
        $change->update();
        return redirect('/surat-masuk');
    }
    
}
