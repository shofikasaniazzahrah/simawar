<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Surat_masuk;
use App\Disposisi;
use App\Surat_keluar;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{


    public function index()
    {

        $get_surat_kadin = Surat_masuk::where('disetujui', '=', 'true')->get();
        $data_user = Auth::user()->id;
        return view('pages.view-surat', compact('data_user','get_surat_kadin'));
        // dump($get_surat_kadin);
    }

    public function user_show()
    {

        $data_user = Auth::user()->id;
        $get_surat_user = Disposisi::where('teruskan', '=', $data_user)->where('bagikan', '=', 'true')->get();
        return view('pages.user-view-surat', compact('data_user','get_surat_user'));
        // dump($get_surat_kadin);
    }

    public function show()
    {
        $datausers = User::all();
        $data_user = Auth::user()->id;
        return view('pages.view-user', compact('data_user','datausers'));
    }

    public function getid($id) 
    {
        $get_id = Surat_masuk::find($id);
        $get_disposisi = Disposisi::where('surat_masuk_id', '=', $id)->first();
        // $get_data_disposisi = DB::table('disposisi')->where('surat_masuk_id', '=', $id)->get();
        $get_data_disposisi = Disposisi::all()->where('surat_masuk_id', '=', $id);
        // dump($get_data_disposisi);
        $data_user = Auth::user()->id;
        $data_jabatan = '';
        return view('pages.detail-surat', compact('get_data_disposisi','data_user','get_id', 'get_disposisi', 'data_jabatan'));
    }

    public function getid_disposisi($id)
    {
        $get_id = Surat_masuk::find($id);
        $get_disposisi = Disposisi::where('surat_masuk_id', '=', $id)->first();
        $data_user = Auth::user()->id;
        return view('pages.detail-disposisi', compact('data_user','get_id', 'get_disposisi'));
    }

    public function create($id)
    {
        // dump($id);
        $data_user = Auth::user()->id;
        return view('pages.form-disposisi', compact('data_user'));;
    }

    public function store(Request $request){


        
        return redirect('/jabatan')->with('berhasil',' Data BaruBerhasil Disimpan!');

    }

    public function destroy($id){
        $query= DB::table('users')->where('id', $id)->delete();
        return redirect('/lihat-user');
    }

    public function edit($id){
        $data_id = User::where('id', $id)->first();
        $data_jabatan = '';
        $data_user = Auth::user()->id;
        return view('pages.edit-user', compact('data_user','data_id', 'data_jabatan'));
    }

     
    public function update($id, Request $request){
        //  dump($request);

        $change = User::find($id);
        $change->id = $request["value"];
        $change->name = $request["name"];
        $change->email = $request["email"];
        // $change->file = $destination;
        $change->update();
        return redirect('/lihat-user');
     }
     public function showsurat(){
         $getsuratkeluar  = Surat_keluar::get();
         $data_user = Auth::user()->id;
         return view('pages.view-suratkeluar', compact('getsuratkeluar','data_user'));
     }
}
