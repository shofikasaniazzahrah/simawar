<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surat_keluar extends Model
{
    protected $table="surat_keluar";
    protected $fillable=["id","nomor_surat","tujuan_surat","perihal","keterangan","file","created_at","update_at"];
}
