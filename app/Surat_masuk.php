<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surat_masuk extends Model
{
    protected $table = "surat_masuk";
    protected $fillable = [
        "id","nomor_surat","asal_surat","tanggal_surat","perihal","keterangan","disetujui","file","created_at","update_at"
    ];

    public function disposisi(){
        return $this->hasOne('App\Disposisi');

    }


}
