@extends('layout.master')

@section('content')

<div class="ml-2 mt-2">
    <a class="btn btn-primary mb-2" href="/surat-masuk"> <i class="nav-icon fas fa-long-arrow-alt-left"> Back</i></a>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title"> Edit Surat Masuk</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/edit/jabatan/{{$data_id->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label for="nomor surat">Nama Jabatan</label>
                    <input value="{{$data_id->nama_jabatan}}" name='nama_jabatan' type="type" class="form-control" id="nomor surat" placeholder="Masukan Nomor Surat">
                </div>

            </div>


    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>

    </div>
    </form>
</div>
</div>

@endsection