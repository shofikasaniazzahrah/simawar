@extends('layout.master')
@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Buat Pengguna Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukan Nama">
                  </div>
                  <div class="form-group">
                    <label for="kode">Kode Pegawai</label>
                    <input type="text" class="form-control" id="nomor" placeholder="Masukan Kode Pegawai">
                  </div>
                  <div class="form-group">
                    <div class="form-group">
                        <label style="default">Bidang (pilih satu)</label>
                        <select class="form-control">
                          <option>PTSP</option>
                          <option>Deregulasi</option>
                          <option>Sistem Informasi</option>
                          <option>Umum dan Kepegawaian</option>
                          <option>Pelayanan</option>
                        </select>
                      </div>
            
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Buat Pengguna Baru</button>
                </div>
              </form>
            </div>
@endsection