@extends('layout.master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Disposisi</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                <th style="width: 5px">No.</th>
                    <th >Nomor Surat</th>
                    <th >teruskan</th>
                    <th>catatan</th>
                    <th >Aksi</th>
                
                </tr>
            </thead>
            <tbody>
            
            @forelse ($getdisposisi->all() as $key=>$data)
                <tr>
                
                    <td>{{ $key+1 }}</td>
                    <td>{{ $data->surat_masuk->nomor_surat }}</td>
                    <td style="width: 300px;">{{ $data->teruskan }}</td>
                    <td style="width: 500px;">{{$data->catatan}} </td>
                    <td>
                        <form action="/view/disposisi/{{$data->surat_masuk->id}}" method="POST">
                            @csrf
                            @method("get")
                            <button> Lihat</button>
                        </form>

                    </td>
<!--                     

                    <td>
                        
                            <form action="/disposisi/{{$data->id}}" method="POST" class="image1 colom-center" >
                            @csrf
                            @method('delete')
                            
                            <button ><i class="fa fa-trash text-center "></i> </button>
                        </form>
                        <form action="edit/disposisi/{{$data->id}}" method="POST"class="image2">
                            @csrf
                            @method("get")
                            <button  ><i class="fas fa-edit"></i> </button>
                
                        </form>

                        
                    </td> -->
                    @empty
                    <tr colspan="3">
                        <td>No data</td>
                            
              
                    </tr>  
               
                    @endforelse
           
           
            </tbody>
            
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
    </div>
</div>
        
    </div> -->
  
@endsection