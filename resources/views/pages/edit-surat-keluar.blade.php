@extends('layout.master')

@section('content')
<div class= "ml-2 mt-2">
<a  class="btn btn-primary mb-2" href="/surat-keluar" > <i class="nav-icon fas fa-long-arrow-alt-left"> Back</i></a>
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Surat Keluar</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/edit/surat-keluar/{{$data_id->id}}" method="POST" enctype="multipart/form-data">
                @csrf 
                @method('put')
                <div class="card-body">
                
                <div class="form-group">
                    <label for="nomor surat">Nomor Surat</label>
                    <input name='nomor_surat' value="{{$data_id->nomor_surat}}" type="text" class="form-control" id="nomor surat" placeholder="Masukan Nomor Surat">
                  </div>

                  <div class="form-group">
                    <label for="tujuansurat">Tujuan Surat</label>
                    <input name='tujuan_surat' value="{{$data_id->tujuan_surat}}"type="text" class="form-control" id="tujuansurta" placeholder="Masukan Tujuan Surat">
                  </div>
                  
                  <div class="form-group">
                    <label for="prihalsurat">Perihal Surat</label>
                    <input name='perihal'value="{{$data_id->perihal}}" type="text" class="form-control" id="prihalsurat" placeholder="Masukan Perihal Surat">
                  </div>
                  <div class="form-group">
                    <label for="Keterangan">Keterangan</label>
                    <input name='keterangan'value="{{$data_id->keterangan}}" type="text" class="form-control" id="keterangan" placeholder="Keterangan">
                  </div>
                


                  <div class="form-group">
                    <label for="file">Nama File</label>
                    <p>{{$data_id->file}}
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Edit File
                        </button>

                    </p>
                </div>


                <small>
                    **jika hanya ingin mengganti file
                </small>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/edit/keluar-file/{{$data_id->id}}" method="POST" class="modal-body" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="exampleInputFile">Ganti File</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input name='file' type="file" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" value="Edit File" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection