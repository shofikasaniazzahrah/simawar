@extends('layout.master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Daftar Surat Keluar</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('berhasil'))
        <div class="alert alert-success">
            {{session('berhasil')}}
        </div>
        @endif


        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th style="width: 5px">No.</th>
                    <th style="width: 80px">Terdaftar</th>
                   
                    <th style="width: 80px">Nomor Surat</th>
                    <th style="width: 50px">Tujuan Surat </th>
                    <th style="width: 100px">Perihal </th>
                    

                    <th style="width: 50px">File </th>
                   

                </tr>
            </thead>
            <tbody>


                @forelse ($getsuratkeluar as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td> <span class="date">{{$value->created_at}}</span></td>
                   
                    <td>{{$value->nomor_surat}}</td>
                    <td>{{$value->tujuan_surat}}</td>
                   
                    <td> {{$value->perihal}}</td>
                  

                    <td> 

                        <form action="/downloadfile" method="POST" class="mt-3 text-center">
                            @csrf
                            <input type="text " name="file" value="{{$value->file}}" class="d" hidden>
                            <button class="btn btn-primary btn-sm btn-block"><i class="fas fa-download"></i> </button>
                        </form>


                    </td>

                   

                </tr>
                @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
                @endforelse






            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
    </div>
</div>

@endsection