@extends('layout.master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Daftar Surat Masuk</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('berhasil'))
        <div class="alert alert-success">
            {{session('berhasil')}}
        </div>
        @endif


        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th style="width: 5px">No.</th>
                    <th style="width: 80px">Terdaftar</th>
                    <th style="width: 80px">Asal Surat</th>
                    <th style="width: 80px">Nomor Surat</th>
                    <th style="width: 50px">Tanggal surat </th>
                    <th style="width: 100px">Perihal </th>
                    <th style="width: 100px">Keterangan </th>

                    <th style="width: 50px">File </th>
                    <!-- <th style="width: 50px"> Disposisi </th> -->

                    <!-- <th style="width: 80px">Disposisi</th>
                 -->
                </tr>
            </thead>
            <tbody>


                @forelse ($get_surat_user as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td> <span class="date">{{$value->surat_masuk->created_at}}</span></td>
                    <td>{{$value->surat_masuk->asal_surat}} </td>
                    <td>{{$value->surat_masuk->nomor_surat}}</td>
                    <td> <span class="date">{{$value->surat_masuk->tanggal_surat}}</span></td>
                    <td> {{$value->surat_masuk->perihal}}</td>
                    <td> {{$value->surat_masuk->keterangan}}</td>

                    <td> <img src="{{ asset('/storage/'.$value->surat_masuk->file) }} " class='gambar' />

                        <form action="/downloadfile" method="POST" class="mt-3 text-center">
                            @csrf
                            <input type="text " name="file" value="{{$value->surat_masuk->file}}" class="d" hidden>
                            <button>Download </button>
                        </form>


                    </td>

                    <!-- <td>
                        <form action="/view/surat/{{$value->id}}" method="POST">
                            @csrf
                            @method("get")
                            <button> Lihat</button>
                        </form>

                    </td> -->

                </tr>
                @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
                @endforelse






            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
    </div>
</div>

@endsection