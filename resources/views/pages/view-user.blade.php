@extends('layout.master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Surat Masuk</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('berhasil'))
        <div class="alert alert-success">
            {{session('berhasil')}}
        </div>
        @endif

        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th style="width: 5px">No.</th>
                    <th style="width: 80px">Name</th>
                    <th style="width: 80px">Email</th>
                    <th style="width: 80px">Jabatan</th>

                    <th style="width: 80px">Aksi</th>

                </tr>
            </thead>
            <tbody>


                @forelse ($datausers as $key=>$value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td> <span class="date">{{$value->name}}</span></td>
                    <td> <span class="date">{{$value->email}}</span></td>
                    <td> <span class="date">{{$value->jabatan->nama_jabatan}}</span></td>
                    <td>

                        <form action="/user/{{$value->id}}" method="POST" >
                            @csrf
                            @method('delete')

                            <button class="btn btn-block btn-primary"><i class="fa fa-trash "></i> </button>

                        </form>
                        <br/>
                        <form action="/edit/user/{{$value->id}}" method="POST" >
                            @csrf
                            @method("get")
                            <button class="btn btn-block btn-primary"><i class="fas fa-edit"></i> </button>

                        </form>
                    </td>
                    <!-- <td>
                        <form action="/disposisi" method="POST"  >
                            @csrf
                            @method("get")
                            <button > Disposisi </button> </form>
                        
                        </td> -->

                </tr>
                @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
                @endforelse






            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
    </div>
</div>

@endsection