@extends('layout.master')

@section('content')
<div class="ml-2 mt-5">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title"> Input Jabatan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/create/jabatan" method="POST" enctype="multipart/form-data">
            @csrf
            @method('POST')
            <div class="card-body">

                <div class="form-group">
                    <label for="nomor surat">Nama Jabatan</label>
                    <input name='nama_jabatan' type="type" class="form-control" id="nomor surat" placeholder="Masukan Nomor Surat" value="{{old('nomor_surat')}}">
                    @error('nomor_surat')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>

            </div>
        </form>
    </div>
</div>

@endsection