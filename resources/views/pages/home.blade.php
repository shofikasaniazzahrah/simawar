@extends('layout.master')
@section('content')
<div class="row ">
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>{{$jmlh_surat_masuk}}</h3>

        <p> Surat Masuk</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      {!! $data_user === 1 ? '<a href="/surat-masuk" class="small-box-footer">Selengkapnya.. <i class="fas fa-arrow-circle-right"></i></a>' : "" !!}
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    
    <!-- small box -->
    <div class="small-box bg-success">
      <div class="inner">
      
        <h3>{{$jmlh_surat_keluar}}<sup style="font-size: 20px"></sup></h3>

        <p> Surat Keluar</p>
      </div>
      <div class="icon">
        
        <i class="ion ion-stats-bars"></i>
      </div>
      {!! $data_user === 1 ? '<a href="/surat-keluar" class="small-box-footer">Selangkapnya.. <i class="fas fa-arrow-circle-right"></i></a>' : "" !!}
      
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-warning">
      <div class="inner">
        <h3>{{$jmlh_disposisi}}<sup style="font-size: 20px"></sup></h3>

        <p>Disposisi</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      {!! $data_user === 1 ? '<a href="/disposisi" class="small-box-footer">Selangkapnya.. <i class="fas fa-arrow-circle-right"></i></a>' : "" !!}
    </div>
  </div>
  <!-- ./col -->
  <!-- <div class="col-lg-3 col-6">
    <div class="small-box bg-danger">
      <div class="inner">
        <h3>{{ $jmlh_users }}</h3>

        <p> User</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      {!! $data_user === 2 ? '<a href="/lihat-user" class="small-box-footer">Selangkapnya.. <i class="fas fa-arrow-circle-right"></i></a>' : "" !!}
    </div>
  </div> -->

</div>

@endsection