@extends('layout.master')

@section('content')

<div class="ml-2 mt-2">
<a  class="btn btn-primary mb-2" href="/surat-masuk" > <i class="nav-icon fas fa-long-arrow-alt-left"> Back</i></a>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title"> Edit Surat Masuk</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/edit/surat-masuk/{{$data_id->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="card-body">
                <label >Diteruskan ke Kepala Dinas? :</label>
                <p >
                   Ya <Input  name="disetujui" type="checkbox" value="true" {{$data_id->disetujui==='true'? "checked" : ""}}/>
                   Tidak <Input  name="disetujui" type="checkbox" value="false" {{$data_id->disetujui==='false'? "checked" : ""}} />
                </p>
                <div class="form-group">
                    <label for="nomor surat">Nomor Surat</label>
                    <input value="{{$data_id->nomor_surat}}" name='nomor_surat' type="type" class="form-control" id="nomor surat" placeholder="Masukan Nomor Surat">
                </div>

                <div class="form-group">
                    <label for="asalsurat">Asal Surat</label>
                    <input value="{{$data_id->asal_surat}}" name='asal_surat' type="type" class="form-control" id="asalsurat" placeholder="asal surat">
                </div>
                <div class="form-group">
                    <label for="tanggalsurat">Tanggal Surat Diterbitkan</label>
                    <input value="{{$data_id->tanggal_surat}}" name='tanggal_surat' type="date" class="form-control" id="tanggalsurat" placeholder="tanggal diterbitkan surat">
                </div>
                <div class="form-group">
                    <label for="Perihal">Perihal</label>
                    <input value="{{$data_id->perihal}}" name='perihal' type="type" class="form-control" id="perihal" placeholder="perihal">
                </div>
                <div class="form-group">
                    <label for="Keterangan">Keterangan</label>
                    <input value="{{$data_id->keterangan}}" name='keterangan' type="type" class="form-control" id="keterangan" placeholder="Keterangan">
                </div>

                <div class="form-group">
                    <label for="file">Nama File</label>
                    <p>{{$data_id->file}}
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Edit File
                        </button>

                    </p>
                </div>


                <small>
                    **jika hanya ingin mengganti file
                </small>






            </div>


    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>

    </div>
    </form>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/edit/masuk-file/{{$data_id->id}}" method="POST" class="modal-body" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="exampleInputFile">Ganti File</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input name='file' type="file" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" value="Edit File" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>
    @endsection