@extends('layout.master')
@section('content')
<div class= "ml-2 mt-5">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Disposisi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/create/disposisi" method="POST" >
                @csrf 
                @method('POST')
                <div class="card-body">
                
                  <div class="form-group">
                    <label  for="teruskan">Teruskan </label>
                    <input  name='teruskan' type="type" class="form-control @error('teruskan') is-invalid @enderror" id="teruskan" placeholder="Teruskan ke"  value="{{old('teruskan,'')}}">
                    @error('teruskan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                      </div>
                  

                  <div class="form-group">
                    <label for="catatan">Catatan</label>
                    <input name='catatan' type="type" class="form-control @error('catatan') is-invalid @enderror" id="catatan" placeholder="catatan">
                    @error('catatan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bagikan">Bagikan</label>
                    <input  name='bagikan'class="form-control @error('bagikan') is-invalid @enderror" id="bagikan" placeholder="bagikan">
                    @error('bagikan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                 
                
                
                
                
                  </div>
                  
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  
                </div>
              </form>
            </div>
</div>

@endsection