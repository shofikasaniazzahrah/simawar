@extends('layout.master')

@section('content')
<div class= "ml-2 mt-5">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Input Surat Keluar</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/create/surat-keluar" method="POST" enctype="multipart/form-data">
                @csrf 
                @method('POST')
                <div class="card-body">
                
                <div class="form-group">
                    <label for="nomor surat">Nomor Surat</label>
                    <input name='nomor_surat' type="text" class="form-control" id="nomor surat" placeholder="Masukan Nomor Surat" value="{{old('nomor_surat', '')}}">
                    @error('nomor_surat')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="tujuansurat">Tujuan Surat</label>
                    <input name='tujuan_surat'type="text" class="form-control" id="tujuansurta" placeholder="Masukan Tujuan Surat"value="{{old('tujuan_surat', '')}}">
                    @error('tujuan_surat')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  
                  <div class="form-group">
                    <label for="prihalsurat">Perihal Surat</label>
                    <input name='perihal' type="text" class="form-control" id="prihalsurat" placeholder="Masukan Perihal Surat" value="{{old('perihal','')}}">
                    @error('perihal')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="Keterangan">Keterangan</label>
                    <input name='keterangan' type="text" class="form-control" id="keterangan" placeholder="Keterangan" value="{{old('keterangan','')}}">
                    @error('keterangan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                


                  <div class="form-group">
                    <label for="exampleInputFile">Input File</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name='file' type="file" class="custom-file-input" id="exampleInputFile" value="{{old('file')}}">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                    @error('file')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>


@endsection