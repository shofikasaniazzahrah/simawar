@extends('layout.master')
@section('content')

<h3 class="border mt-3 mb-2">Data Surat Masuk</h3>

<div class="container text-left">
    <div class="row text-left">
        <div class="col ">Nomor Agenda</div>
        <div class="col">:{{$get_id->id}}</div>
        <div class="w-100"></div>
        <div class="col">Terdaftar</div>
        <div class="col">:{{$get_id->created_at}}</div>
        <div class="w-100"></div>
        <div class="col">Nomor Surat</div>
        <div class="col">:{{$get_id->nomor_surat}}</div>
        <div class="w-100"></div>
        <div class="col">Asal Surat</div>
        <div class="col">:{{$get_id->asal_surat}}</div>
        <div class="w-100"></div>
        <div class="col">Tanggal Surat Diterbitkan Surat</div>
        <div class="col">:{{$get_id->tanggal_surat}}</div>
        <div class="w-100"></div>
        <div class="col">Perihal</div>
        <div class="col">:{{$get_id->perihal}}</div>
        <div class="w-100"></div>


    </div>
</div>


<hr />

<h3 class="border mt-3 mb-2">Disposisi </h3>
@if( $get_disposisi !== null )
<!-- <p> {{$get_disposisi->teruskan}} </p> -->
<table class="table table-bordered  text-left">
    <thead>
        <tr>

            <th>Nomor Surat</th>
            <th>teruskan</th>
            <th>catatan</th>
            <th>Aksi</th>

        </tr>
    </thead>
    <tbody>


        @forelse ($get_data_disposisi as $key=>$value)
        <tr>
            <td>{{ $value->surat_masuk->nomor_surat }}</td>
            <td style="width: 300px;">{{ $value->teruskan }}</td>
            <td style="width: 500px;">{{$value->catatan}} </td>

            <td>

                <form action="{{  route('delete_disposisi', $value->id)  }}" method="POST" class="image1">
                    @csrf
                    @method('delete')

                    <button><i class="fa fa-trash "></i> </button>
                </form>
                <form action="{{ route('getid_disposisi', $value->id) }} " method="POST" class="image2">
                    @csrf
                    @method("get")
                    <button><i class="fas fa-edit"></i> </button>

                </form>


            </td>

        </tr>
        @empty
        <tr colspan="3">
            <td>No data</td>
        </tr>
        @endforelse




    </tbody>

</table>



@else
<div class="card">
    <div class="card-body">
        <h5 class="mt-2  mb-2"> Disposisi belum dibuat</h5>
        <small>Silahkan buat dibawah ini</small>
    </div>
</div>

<div class="ml-2 mt-5">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Buat Disposisi</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/create/disposisi" method="POST">
            @csrf
            @method('POST')
            <div class="card-body">

                <input name='id_surat' value="{{ $get_id->id }}" hidden type="type" class="form-control" id="teruskan" placeholder="Teruskan ke">


                <div Fclass="form-group">
                    <label for="catatan">Catatan</label>
                    <!-- <input name='catatan' type="type" class="form-control" id="catatan" placeholder="catatan"> -->
                    <textarea name="catatan" class="form-control"></textarea>
                    @error('catatan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <br />


                <div Fclass="form-group">
                    <label for="catatan">Teruskan ke :</label>
                    <br />
                    <input name='teruskan[]' type="checkbox" value="Sekretaris">Sekretaris<br />
                    <input name='teruskan[]' type="checkbox" value="Kabid Pelayanan Terpadu Satu Pintu">Kabid Pelayanan Terpadu Satu Pintu<br />
                    <input name='teruskan[]' type="checkbox" value="Kabid Sistem Informasi Evaluasi dan Pengaduan Layanan">Kabid Sistem Informasi Evaluasi dan Pengaduan Layanan<br />
                    <input name='teruskan[]' type="checkbox" value="Kabid Pengendalian layanan informasi">Kabid Pengendalian layanan informasi<br />
                    <input name='teruskan[]' type="checkbox" value="Kabid Deregulasi Pengembangan Iklim dan Promosi Penanaman Modal">Kabid Deregulasi Pengembangan Iklim dan Promosi Penanaman Modal<br />
                    <input name='teruskan[]' type="checkbox" value="Kasubag">Kasubag<br />
                    <input name='teruskan[]' type="checkbox" value="Kasi-kasi">Kasi-kasi<br />
                    <input name='teruskan[]' type="checkbox" value="Staff">Staff<br />
                    <br />
                    @error('catatan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


            </div>


    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>

    </div>
    </form>
</div>
</div>
@endif





@endsection