@extends('layout.master')

@section('content')
<div class="ml-2 mt-2">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Disposisi</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/edit/disposisi/{{$data_id->id}}" method="POST">
      @csrf
      @method('put')
      <div class="card-body">

        <!-- 

        <div class="form-group">
          <label for="catatan">Catatan</label>
          <input name='catatan' type="type" class="form-control" id="catatan" placeholder="catatan" value="{{$data_id->catatan}}">
          @error('catatan')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div> -->

        <div Fclass="form-group">
          <label for="catatan">Catatan</label>
          <!-- <input name='catatan' type="type" class="form-control" id="catatan" placeholder="catatan"> -->
          <textarea name="catatan" class="form-control">{{$data_id->catatan}}</textarea>
          @error('catatan')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>

        <br />

        <div Fclass="form-group">
          <label for="catatan">Teruskan ke :</label>
          @foreach($data_teruskan as $value)
          <br />
          <input name='teruskan[]' type="checkbox" {{ ($value === $value ? 'checked' : '') }} value="{{ $value }}">{{$value}}
          @endforeach
          @foreach($data_tidakteruskan as $e)
          <br />
          <input name='teruskan[]' type="checkbox" {{ ($e === $e ? '' : 'checked') }} value="{{ $e }}">{{$e}}
          @endforeach


          @error('catatan')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>






      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </form>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/edit/file/{{$data_id->id}}" method="POST" class="modal-body" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="exampleInputFile">Ganti File</label>
          <div class="input-group">
            <div class="custom-file">
              <input name='file' type="file" class="custom-file-input" id="exampleInputFile">
              <label class="custom-file-label" for="exampleInputFile">Choose file</label>
            </div>
            <div class="input-group-append">
              <span class="input-group-text">Upload</span>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <input type="submit" value="Edit File" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
</div>
@endsection