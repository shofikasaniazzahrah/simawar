@extends('layout.master')
@section('content')

<h3 class="border mt-3 mb-2">Data Surat Masuk</h3>

<div class="container text-left">
    <div class="row text-left">
        <div class="col ">Nomor Agenda</div>
        <div class="col">:{{$get_id->id}}</div>
        <div class="w-100"></div>
        <div class="col">Terdaftar</div>
        <div class="col">:{{$get_id->created_at}}</div>
        <div class="w-100"></div>
        <div class="col">Nomor Surat</div>
        <div class="col">:{{$get_id->nomor_surat}}</div>
        <div class="w-100"></div>
        <div class="col">Asal Surat</div>
        <div class="col">:{{$get_id->asal_surat}}</div>
        <div class="w-100"></div>
        <div class="col">Tanggal Surat Diterbitkan Surat</div>
        <div class="col">:{{$get_id->tanggal_surat}}</div>
        <div class="w-100"></div>
        <div class="col">Perihal</div>
        <div class="col">:{{$get_id->perihal}}</div>
        <div class="w-100"></div>


    </div>
</div>


<hr />

<h3 class="border mt-3 mb-2">Disposisi </h3>
@if( $get_disposisi !== null )
<!-- <p> {{$get_disposisi->teruskan}} </p> -->
<table class="table table-bordered  text-left">
    <thead>
        <tr>

            <th>Nomor Surat</th>
            <th>teruskan</th>
            <th>catatan</th>

        </tr>
    </thead>
    <tbody>

        <tr>


            <td>{{ $get_disposisi->surat_masuk->nomor_surat }}</td>
            <td style="width: 300px;">{{ $get_disposisi->teruskan }}</td>
            <td style="width: 500px;">{{$get_disposisi->catatan}} </td>



        </tr>




    </tbody>

</table>



@else



<div class="card">
    <div class="card-body">
        <h5 class="mt-2  mb-2"> Disposisi belum dibuat</h5>
        <small>Silahkan buat dibawah ini</small>
    </div>
</div>

</div>

@endif

@endsection