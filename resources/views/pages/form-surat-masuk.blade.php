@extends('layout.master')

@section('content')
<div class= "ml-2 mt-5">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Input Surat Masuk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/create/surat-masuk" method="POST" enctype="multipart/form-data" >
                @csrf 
                @method('POST')
                <div class="card-body">
                
                  <div class="form-group">
                    <label  for="nomor surat">Nomor Surat</label>
                    <input  name='nomor_surat' type="type" class="form-control @error('nomor_surat') is-invalid @enderror" id="nomor surat" placeholder="Masukan Nomor Surat" value="{{old('nomor_surat')}}" >
                    @error('nomor_surat')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                      </div>

                  <div class="form-group">
                    <label for="asalsurat">Asal Surat</label>
                    <input name='asal_surat' type="type" class="form-control @error('asal_surat') is-invalid @enderror" id="asalsurat" placeholder="asal surat"value="{{old('asal_surat')}}">
                    @error('asal_surat')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="tanggalsurat">Tanggal Surat Diterbitkan</label>
                    <input  name='tanggal_surat'type="date" class="form-control @error('tanggal_surat') is-invalid @enderror" id="tanggalsurat" placeholder="tanggal diterbitkan surat" value="{{old('tanggal_surat')}}">
                    @error('tanggal_surat')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="Perihal">Perihal</label>
                    <input name='perihal' type="type" class="form-control @error('perihal') is-invalid @enderror" id="perihal" placeholder="perihal" value="{{old('perihal')}}">
                    @error('perihal')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="Keterangan">Keterangan</label>
                    <input name='keterangan' type="type" class="form-control @error('file') is-invalid @enderror" id="keterangan" placeholder="Keterangan" value="{{old('keterangan')}}">
                    @error('keterangan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
            
              
                  
                  <div class="form-group">
                    <label for="exampleInputFile">Input File</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name='file' type="file" class="custom-file-input" id="exampleInputFile" value="{{old('file')}}">
                        
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                      
                    </div>
                    @error('file')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                
                
                
                
                  </div>
                  
                
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  
                </div>
              </form>
            </div>
</div>

@endsection