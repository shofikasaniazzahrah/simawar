<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    <img src="{{asset('adminlte/dist/img/muaraenim.jpg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">SI-MAWAR</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <!-- <div class="image">
        <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div> -->
      <div class="info">
        <a href="#" class="d-block">Hallo, {!! Auth::user()->name ? Auth::user()->name : 'User' !!}</a>
      </div>
    </div>


    <!-- {{$data_user}} -->
    <nav class="mt-2">
      <!-- <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

        <li class="nav-header">Dashboard</li>
        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p> Dashboard </p>
          </a>
        </li>

        <li class="nav-header">Lihat data</li>
        <li class="nav-item">
          <a href="/surat-masuk" class="nav-link">
            <i class=" nav-icon fas fa-envelope-open-text"></i>
            <p> Surat Masuk </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/surat-keluar" class="nav-link">
            <i class="nav-icon fas fa-paper-plane"></i>
            <p> Surat Keluar </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/disposisi" class="nav-link">
            <i class="nav-icon fas fa-sticky-note"></i>
            <p> Disposisi </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/lihat-user" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p> User </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/jabatan" class="nav-link">
            <i class="nav-icon fas fa-users"></i>
            <p> Jabatan </p>
          </a>
        </li>

        <li class="nav-header">Input data</li>
        <li class="nav-item">
          <a href="/create/surat-masuk" class="nav-link">
            <i class="nav-icon fas fa-edit"></i>
            <p>Input Surat Masuk </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/create/surat-keluar" class="nav-link">
            <i class="nav-icon fas fa-edit"></i>
            <p>Input Surat Keluar </p>
          </a>
        </li>


        <li class="nav-header">Dashboard</li>
        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p> Dashboard </p>
          </a>
        </li>
        <li class="nav-header">Menu Kepala Dinas</li>
        <li class="nav-item">
          <a href="/view/surat" class="nav-link">
            <i class="nav-icon fas fa-eye"></i>
            <p> Lihat Surat </p>
          </a>
        </li>


        <li class="nav-item">
        <li class="nav-header">User</li>
        <li class="nav-item">
          <a href="/user/view-surat" class="nav-link">
            <i class=" nav-icon fas fa-eye"></i>
            <p> Lihat Surat </p>
          </a>
        </li>
              </ul>
    </nav>
         -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          @if($data_user === 1)
          <li class="nav-header">Dashboard</li>

          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p> Dashboard </p>
            </a>
          </li>

          <li class="nav-header">Lihat data</li>
          <li class="nav-item">
            <a href="/surat-masuk" class="nav-link">
              <i class=" nav-icon fas fa-envelope-open-text"></i>
              <p> Surat Masuk </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/surat-keluar" class="nav-link">
              <i class="nav-icon fas fa-paper-plane"></i>
              <p> Surat Keluar </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/disposisi" class="nav-link">
              <i class="nav-icon fas fa-sticky-note"></i>
              <p> Disposisi </p>
            </a>
          </li>
<!-- 
          <li class="nav-item">
            <a href="/lihat-user" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p> User </p>
            </a>
          </li> -->

          <!-- <li class="nav-item">
            <a href="/jabatan" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p> Jabatan </p>
            </a>
          </li> -->

          <li class="nav-header">Input data</li>
          <li class="nav-item">
            <a href="/create/surat-masuk" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>Input Surat Masuk </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/create/surat-keluar" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>Input Surat Keluar </p>
            </a>
          </li>

          @elseif ($data_user > 1)
          <li class="nav-header">Dashboard</li>
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p> Dashboard </p>
            </a>
          </li>
          <li class="nav-header">Menu Surat</li>
          <li class="nav-item">
            <a href="/view/surat" class="nav-link">
              <i class="nav-icon fas fa-eye"></i>
              <p> Lihat Surat Masuk </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/view/surat-keluar" class="nav-link">
              <i class="nav-icon fas fa-eye"></i>
              <p> Lihat Surat Keluar </p>
            </a>
          </li>

          @endif

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>