<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/dashboard', 'DashboardController@index')->middleware('auth');


Route::get('/surat-masuk', 'SuratMasukController@index')->middleware('auth');
Route::get('/create/surat-masuk','SuratMasukController@create')->middleware('auth');
Route::post('/create/surat-masuk','SuratMasukController@store')->middleware('auth');
Route::delete('/surat-masuk/{id}','SuratMasukController@destroy')->middleware('auth');
Route::post('/downloadfile','SuratMasukController@download')->middleware('auth');
Route::get('/edit/surat-masuk/{id}','SuratMasukController@edit')->middleware('auth');
Route::put('/edit/surat-masuk/{id}','SuratMasukController@update')->middleware('auth');
Route::put('/edit/masuk-file/{id}', 'SuratMasukController@updatefile')->middleware('auth');


Route::get('/surat-keluar', 'SuratKeluarController@index')->middleware('auth');
Route::get('/create/surat-keluar','SuratKeluarController@create')->middleware('auth');
Route::post('/create/surat-keluar','SuratKeluarController@store')->middleware('auth');
Route::delete('/surat-keluar/{id}', 'SuratKeluarController@destroy')->middleware('auth');
Route::post('/download_file','SuratKeluarController@download')->middleware('auth');
Route::get('/edit/surat-keluar/{id}','SuratKeluarController@edit')->middleware('auth');
Route::put('/edit/surat-keluar/{id}','SuratKeluarController@update')->middleware('auth');
Route::put('/edit/keluar-file/{id}', 'SuratKeluarController@updatefile')->middleware('auth');




Route::get('/disposisi','DisposisiController@index')->middleware('auth');
Route::delete('/disposisi/{id}', 'DisposisiController@destroy')->name('delete_disposisi')->middleware('auth');;
Route::get('/create/disposisi','DisposisiController@create')->middleware('auth');
Route::post('/create/disposisi','DisposisiController@store')->middleware('auth');
Route::get('/edit/disposisi/{id}','DisposisiController@edit')->name('getid_disposisi')->middleware('auth');
Route::put('/edit/disposisi/{id}','DisposisiController@update')->name('update_disposisi')->middleware('auth');





Route::get('/view/surat','UserController@index')->middleware('auth');
Route::get('/view/surat/{id}','UserController@getid')->middleware('auth');
Route::get('/view/disposisi/{id}','UserController@getid_disposisi')->middleware('auth');
Route::get('/create/disposisi/{id}','UserController@create')->middleware('auth');




Route::get('/lihat-user','UserController@show')->middleware('auth');
Route::delete('/user/{id}','UserController@destroy')->middleware('auth');
Route::get('/edit/user/{id}','UserController@edit')->middleware('auth');
Route::put('/edit/user/{id}','UserController@update')->middleware('auth');
Route::get('/user/view-surat','UserController@user_show')->middleware('auth');
Route::get('view/surat-keluar','UserController@showsurat')->middleware('auth');



Route::get('/login', function(){
    return view('login');
});
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

// Route::get('home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');




Route::middleware(['auth', 'subscribed'])->group(function () {
    return view('login');
});



Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
